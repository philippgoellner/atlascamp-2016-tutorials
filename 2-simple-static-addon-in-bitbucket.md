# Injecting your content into Bitbucket

With Atlassian Connect you can go from nothing to having your content injected into an Atlassian
product in a short amount of time. To prove this to you, this tutorial is going to create a static
add-on that injects some simple content into a Bitbucket repository page and then makes some request
to external third party API's.

## Create a working directory

You are going to write this project from scratch. Start by creating a directory to work in called:

    mkdir bitbucket-static && cd bitbucket-static

This directory will only contain two files: atlassian-connect.json and repo-page.html. This entire 
tutorial will only require the addition and editing of two files for our entire add-on.

## Writing a descriptor for Bitbucket

First we are going to start out with a descriptor that can be installed into Bitbucket. Create a file
called 'atlassian-connect.json' in your working directory and make the initial contents of that
file:

    {
      "key": "bitbucket-connect-static",
      "name": "Tutorial Bitbucket Connect Static Add-on",
      "description": "Example add-on",
      "baseUrl": "https://<to-be-generated>.ngrok.io",
      "authentication": {
         "type": "none"
      },
      "scopes": ["repository"],
      "contexts": ["personal"]
    }

Lets first walk through what this add-on descriptor declares: 

 - The key, name and description are straightforward and operate under the same rules that we have seen before. 
 - The baseUrl is important and will point to an ngrok URL that we will generate in the next section. 
 - We don't need to authenticate anything because we are not going to have a backend server or make any requests to or from Bitbucket; this
   example is purely about getting our content into Bitbucket. 
 - We actually do not need to request any scopes but, if we omit the scopes section entirely, Bitbucket assumes that we meant to request all
   scopes; which is not what we want at all. Instead we request repository scope just so that we have
   as few scopes as possible. 
 - Finally, we want to install this add-on against our own personal accounts so we declare that the context that this add-on can be installed in is the 'personal' context.

We now have an add-on, that declares no modules, that we could install into Bitbucket. Now it is time to
make it avaliable on the public internet.

## Hosting your static add-on

Currently we only have a single static file but, in order to turn that into a static add-on you need
to serve those files via a HTTP webserver. Here you have a number of options.

### Using node http-server

If you already have npm installed then you can just install [http-server][1] and use that. In your
base directory:

    $ npm install http-server
    $ ln -s node_modules/.bin/http-server .
    $ ./http-server -p 8011

### Using python HttpServer

If you have python installed you can also use that to generate a HTTP Server in your
bitbucket-static base directory. Just do the following:

    $ python -m SimpleHTTPServer 8011 

### Running ngrok

If you have successfully started up a HTTP server on port 8011 then you should be able to navigate to
http://localhost:8011/atlassian-connect.json and see the descriptor that you wrote in the previous
step.

You then need to make this descriptor avaliable on the public internet. Assuming that [you
successfully installed ngrok in the previous tutorial][4] you should just be able to run the following in
another terminal window:

    $ ngrok http 8011

When this works you should recieve a HTTPS url through which you can access
https://<ngrok-url>/atlassian-connect.json. However, when you visit your atlassian connect descriptor, 
you will notice that the baseUrl still has not been injected correctly. Replace the 
baseUrl in your descriptor with your newly generated ngrok url.

Congratulations, your add-on is now hosted: ready to be installed into Bitbucket.

## Installing a personal Bitbucket add-on

To install a personal Bitbucket addon:

 1. Navigate to https://bitbucket.org and login with your credeentials.
 1. In the top right will be an avatar representing your account: click on that icon and select
    "Integrations" from the dropdown menu.
 1. After the "Find integrations" page has loaded click on "Manage integrations" in the bottom left of the
    sidebar.
 1. Once the "Manage integrations" page has loaded you should see a "Install from add-on URL" link; click
    on that link.
 1. In the dialog box that appears paste your HTTPS ngrok link to your atlassian-connect descriptor
    and click "Install".
 1. Since you have requested 'repository' scope another dialog will appear asking you to grant this
    add-on 'repository' access. Click "Grant access".

Awesome! Your add-on is now installed into Bitbucket, but it is a rather boring add-on because it
does not do anything yet! This is because it declares no add-on modules. Let's update the add-on so
that it adds a page to every repository that we visit.

## Writing a repository page

The first thing that we should do is create the page that should be loaded into our repositories
iframe. Create a file in your base directory called 'repo-page.html' and fill it with the following 
contents:

    <!DOCTYPE html>
    <html lang="en">
       <head>
          <!-- Include jQuery -->
          <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    
          <!-- Include the Atlassian User Interface (AUI) to make our iframe beautiful -->
          <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.7.0/css/aui.css" media="all">
          <script src="//aui-cdn.atlassian.com/aui-adg/5.7.0/js/aui.js"></script>
    
          <!-- Required All.js downloaded from Bitbucket directly -->
          <script src="https://bitbucket.org/atlassian-connect/all.js"></script>
    
          <!-- A 'nice to have' style block that makes the background of our iframe white -->
          <style>
             body {
                background-color: white;
             }
          </style>
       </head>
       <body>
          <img src="https://upload.wikimedia.org/wikipedia/commons/f/f7/Who-is-awesome.jpg">
       </body>
    </head>

This is a pretty simple HTML page. It includes jQuery, AUI and the Bitbucket all.js and has a single 
image in the page body; but this is the page that we are going to make render in Bitbucket. 

In order to add this page to Bitbucket we need to update our descriptor to declare a new repository page. 
To do that we are going to need a [repoPage module][2]; that looks like so:

    {
       ...
       "modules": {
          "repoPage": [{
             "key": "my-repo-page",
             "name": {
                "value": "Tutorial Repo Page"
             },
             "url": "/repo-page.html",
             "location": "org.bitbucket.repository.navigation",
             "params": {
                "auiIcon": "aui-iconfont-pages"
             }
          }]
       }
       ...
    }
    
Merge this snippet into your atlassian-connect.json file.

This module descriptor says that we have declared that we want a new page to appear against every
repository that we visit. The link in the left sidebar to the page should be called "Tutorial Repo
Page" and the icon that should be to the left of the link name should be the pages AUI icon.
Finally, the HTML content that should be loaded into the iframe that is the repo page should be
loaded from the '/repo-page.html' url relative to our add-ons base url.

__Important:__ Since you have updated your Atlassian Connect descriptor you will need to go back to the "Manage
add-ons" page against your Bitbucket profile and click the 'Update' button on our add-on; that will cause
Bitbucket to fetch and reinstall your add-on against your profile.

Once you have updated your add-on you should be able to navigate to any of your Bitbucket
repositories and see the "Tutorial Repo Page" link in the repositories left sidebar navigation.
Click on that link and you should be presented with this: 

![You're awesome](https://s3.amazonaws.com/uploads.hipchat.com/10804/83693/vVwiASTHlj1xHnS/Screen%20Shot%202016-05-18%20at%203.06.10%20PM.png)

## Making requests to external services

Now that we have managed to inject our first piece of static content into a repository page there is a 
great oppourtunity to integrate other external services with our add-on. For the next part of the
demo we are going to integrate our new repo page with a rest api from [Mashape][2]; this rest api is
going to let us search through npm packages.

Lets start by adding the required UI elements to our repo-page.html. In the body section of the repo
page html add in the following snippet:

    <form class="aui">
       <fieldset>
          <div class="field-group">
             <label for="d-fname">Search npm for</label>
             <input class="text" id="npm-search-text" title="NPM search text" type="text">
             <input class="button npm-search-button" value="Search" id="npm-search-button" type="submit">
             
             <div class="description">Search for a package in npm by name.</div>
          </div>
       </fieldset>
    </form>
    <ol id="search-results"></ol>

This snippet creates a form that expects you to type in an npm package with this implication that 
it will search for a package with this name in npm for you when you hit the 'Search' button.

Now we want to add the required javascript to make our new form do something. Add this snippet into the head
section of your repo-page.html:

      <script type="text/javascript">
         // This function uses the Neutrino Api (through Mashape) to call the NPM search code and return the packages that it finds.
         // It does all of this by making a standard jQuery rest API call.
         var npmSearch = function(searchTerm) {
            return AJS.$.ajax({
               url: 'https://neutrinoapi-html-extract.p.mashape.com/html-extract-tags',
               type: 'GET',
               data: {
                  tag: 'div.package-details .name',
                  content: 'https://www.npmjs.com/search?q=' + searchTerm
               },
               dataType: 'json',
               headers: {
                  "X-Mashape-Key": "8zIzdBQ61XmshaXeFYnnJ0SEr5HEp1PQcsIjsnRcAwHrpNIi7J"
               },
               crossDomain: true,
               cache: false
            });
         };

         AJS.$(function() {
            AJS.$('#npm-search-button').click(function(e) {
               e.preventDefault();
               // When the search button is clicked, grab the search text and search for that on NPM and pass the returned packages into the provided callback function
               npmSearch(AJS.$('#npm-search-text').val()).done(function(npmPackages) {
                  var resultsList = AJS.$("#search-results");
                  // Clear the list of previous results
                  resultsList.empty();
                  // For each package add it to the list of results
                  AJS.$.each(npmPackages.values, function(i, packageName) {
                     resultsList.append('<li><a href="https://www.npmjs.com/package/' + packageName + '">' + packageName + '</a></li>');
                  });
               });
            });
         });
      </script>

This script is pretty simple. The first npmSearch function that is declared merely uses the Neutrino 
API (through Mashape) to extract package information from the NPM search. The second half of the
code snippet makes it so that the search button click event is registered to perform the NPM search
on the textual input that the user entered, and renders the search results to the page.

Since you have made no changes to the Atlassian Connect descriptor you do not need to go to the
"Manage add-ons" page and click the update button. Instead, when you save repo-page.html, the effects
take place immediately so you can refresh the repo page in Bitbucket and see the changes.

Congratulations! You have officially written an add-on for Bitbucket that has a
UI integration and can add value to the Bitbucket experience.

![Npm search](https://s3.amazonaws.com/uploads.hipchat.com/10804/83693/KjOZDaY8yNjadOC/Screen%20Shot%202016-05-18%20at%203.59.35%20PM.png)

## Conclusion

From here you can expand this Bitbucket integration to be anything that you want. Have fun playing
around with Bitbucket by adding external content to it. 

If you wish to query the Bitbucket API directly in your integration then you can discover
how to do that in [the Bitbucket getting started guide][3].

 [1]: https://www.npmjs.com/package/http-server
 [2]: https://www.mashape.com/
 [3]: https://developer.atlassian.com/bitbucket/guides/getting-started.html
 [4]: https://developer.atlassian.com/static/connect/docs/latest/developing/developing-locally-ngrok.html
